package guipassword;

/**
 *
 * @author VICTOR OMONDI;
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener; //Interface
import java.awt.event.ActionEvent; //Class 


public class GUIPassword extends JFrame implements ActionListener{
    private JLabel lblUserName, 
                   lblPassword,
                   lblInputValue,
                   lblResult;
    private JTextField txtUserName,txtInputValue,txtResult = new JTextField(17);
    private JButton btnLogIn,
                    btnExit,
                    btnSquare,
                    btnEvenOrOdd;
    private JPasswordField pswUserPassword;
    
    public GUIPassword(){
    lblUserName = new JLabel("User Name: ");
    lblPassword = new JLabel("Password: ");
    pswUserPassword = new JPasswordField(15);
    btnLogIn = new JButton("Log In");
    btnExit = new JButton("Exit");
    
    //Set Frame Properties
    setVisible(true);
    setSize(500, 300);
    setLayout(new FlowLayout());
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setTitle("=====VICK VENTURES TECHPRENEURSHIP=====");
    
    //Add Components to the frame
    add(lblUserName);
    add(txtUserName);
    add(lblPassword);
    add(pswUserPassword);
    add(btnLogIn);
    add(btnExit);
    add(lblInputValue);
    add(txtInputValue);
    add(lblResult);
    add(txtResult);
    add(btnSquare);
    add(btnEvenOrOdd);
    
    //Add Action Listerner to the buttons
    btnLogIn.addActionListener(this);
    btnExit.addActionListener(new ActionListener(){
       public void actionPerformed(ActionEvent e){
           int x = JOptionPane.showConfirmDialog(null, "=====VICK VENTURES TECHPRENEURSHIP=====\n\n\nExit The Program??");
           if(x==0){
               System.exit(0);
        }
       } 
    });
}
    
    @Override
    public void actionPerformed(ActionEvent e){
        String userName, 
               userPassword;
        userName = txtUserName.getText();
        userPassword = pswUserPassword.getText();
        
        if(userName.equalsIgnoreCase("Victor Omondi") && userPassword.equals("VickTheGeek")){
            JOptionPane.showMessageDialog(null, "=====VICK VENTURES TECHPRENEURSHIP=====\n\n\nWelcome "+userName);
        }else{
            JOptionPane.showMessageDialog(null, "=====VICK VENTURES TECHPRENEURSHIP=====\n\n\nTYPE MISMATCH\nNOT!! "+userName+"\nTry Again");
            txtUserName.setText("Victor Omondi");
            pswUserPassword.setText("VickTheGeek");
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new GUIPassword();
    }
}
